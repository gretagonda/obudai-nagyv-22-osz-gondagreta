package hu.oe.WarehouseApplication.persistance.repository;

import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface StorageRoomRepository extends CrudRepository<StorageRoom, Long> {

    Iterable<StorageRoom> findAll();

    Optional<StorageRoom> findById(Long id);
}
