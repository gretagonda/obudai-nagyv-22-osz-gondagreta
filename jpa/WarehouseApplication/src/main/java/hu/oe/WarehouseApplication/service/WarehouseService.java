package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.persistance.repository.BoxRepository;
import hu.oe.WarehouseApplication.persistance.repository.CostumerRepository;
import hu.oe.WarehouseApplication.persistance.repository.StorageRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.io.IOException;

@Component
public class WarehouseService implements  Service{

    @Autowired
    private BoxRepository boxRepository;
    @Autowired
    private CostumerRepository costumerRepository;
    @Autowired
    private StorageRoomRepository storageRoomRepository;


    private IsLogged isLogged;

    public WarehouseService()
    {
        this.isLogged = new IsLogged();

    }

    public IsLogged getIsLogged() {
        return isLogged;
    }

    public void setIsLogged(IsLogged isLogged) {
        this.isLogged = isLogged;
    }

    public Iterable<StorageRoom> getAllStorageRooms()
    {
        return  storageRoomRepository.findAll();
    }

    public void getStorageRoomsToCostumers()
    {
        Iterable<Costumer> costumers = costumerRepository.findAll();
        Iterable<StorageRoom> storageRooms = storageRoomRepository.findAll();

        for(Costumer c : costumers)
        {
            for(StorageRoom sr : storageRooms)
            {
                if(sr.getCostumer()!=null) {
                    if (c.getCostumerId().equals(sr.getCostumerId())) {
                        isLogged.getCostumer().addStorageRoom(sr);
                        Costumer cost = costumerRepository.findById(c.getCostumerId()).get();
                        cost.addStorageRoom(sr);
                        costumerRepository.save(cost);
                    }
                }
            }
        }

    }

    @Override
    public boolean authenticate(String username, String password) {

        Iterable<Costumer> users = costumerRepository.findAll();
        for(Costumer user : users) {
                if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                    isLogged.setLoggedIn(true);
                    isLogged.setCostumer(user);
                    return true;
                }
        }

        return false;
    }

    @Override
    public boolean isLoggedIn() {
        if(isLogged.isLoggedIn()==true)
        {
            return true;
        }
        return false;
    }

    @Override
    public Costumer getLoggedInCostumer() {
        if(isLogged.isLoggedIn()==true)
        {
            return isLogged.getCostumer();
        }
        return null;
    }

    public Costumer getLoggedInCostumerFromDatabase() {
        if(isLogged.isLoggedIn()==true)
        {
            Costumer costumer = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();

            return costumer;
        }
        return null;
    }

    @Override
    public void logout() throws IOException {
        //this.saveData();
        isLogged.setLoggedIn(false);

    }

    @Override
    public void rentStorageRoom(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        StorageRoom sr2 = new StorageRoom();
        sr.setFree(false);
        sr.setCostumer(isLogged.getCostumer());
        storageRoomRepository.save(sr);
        isLogged.getCostumer().addStorageRoom(sr);
        Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
        cst.addStorageRoom(sr);
        costumerRepository.save(cst);

    }

    @Override
    public void cancelStorageRoomRenting(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        isLogged.getCostumer().removeStorageRoomById(storageRoomId);
        Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
        cst.removeStorageRoomById(storageRoomId);
        sr.setFree(true);
        sr.setCostumer(null);
        storageRoomRepository.save(sr);
        costumerRepository.save(cst);

    }

    @Transactional
    @Override
    public void storeRoom(Box box, Long storageRoomId) {


        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        sr.addBox(box);
        boxRepository.save(box);
        storageRoomRepository.save(sr);
        for(StorageRoom room : isLogged.getCostumer().getStorageRooms())
        {
            if(room.getStorageRoomId().equals(storageRoomId))
            {
                room.addBox(box);
            }
        }
    }

    @Override
    public void removeBox(Long boxId) {
        Box bx = boxRepository.findById(boxId).get();
        boxRepository.delete(bx);

        for(int i = 0; i < isLogged.getCostumer().getStorageRooms().size(); i++)
        {
            for(int j = 0; j < isLogged.getCostumer().getStorageRooms().get(i).getBoxes().size(); j++)
            {
                Box b = isLogged.getCostumer().getStorageRooms().get(i).getBoxes().get(j);
                if(b.getBoxId().equals(boxId))
                {
                    isLogged.getCostumer().getStorageRooms().get(i).removeBox(b);
                }
            }
        }

    }

    @Override
    public boolean isStorageRoomRentable(Long id) {

        StorageRoom storageRoom = storageRoomRepository.findById(id).get();
        if(storageRoom.isFree())
        {
                return true;
        }else {

            return false;
        }
    }

    public  StorageRoom getStorageRoomByIs(Long id)
    {
        return storageRoomRepository.findById(id).get();
    }

}
