package hu.oe.WarehouseApplication.persistance;

public enum Category {
    FLAMMABLE,
    LIGHT,
    HEAVY,
    FRAGILE
}
