package hu.oe.WarehouseApplication.persistance;

public enum Material {
    GLASS,
    TIMBER,
    METAL,
    PAPER
}
