package hu.oe.WarehouseApplication;



import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.service.TestDataGenerator;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.view.WarehouseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class App {

    private WarehouseView warehouseView;

    private WarehouseService warehouseService;

    private  TestDataGenerator testDataGenerator;

    @Autowired
    public App(WarehouseView warehouseView, WarehouseService warehouseService , TestDataGenerator testDataGenerator)
    {
        this.warehouseView = warehouseView;
        this.warehouseService = warehouseService;
        this.testDataGenerator = testDataGenerator;


    }

    public App()
    {

    }


/*
    public void start() {
        warehouseView.printWelcomeMessage();
        printAllStorageRoomDetails();
    }
*/

    public void start() {
        warehouseView.printWelcomeMessage();
        //testDataGenerator.createTestData();
        initCostumerList();

        this.login();
    }

    public void initCostumerList()
    {
        warehouseService.getStorageRoomsToCostumers();

    }
    private void login() {
        String username = warehouseView.readUsername();
        String password = warehouseView.readPassword();
        //List<StorageRoom> sr = warehouse.getStorageRooms();
        boolean isAuthenticated = warehouseService.authenticate(username, password);
        if(isAuthenticated == true)
        {
            warehouseView.printAuthenticatedMessage(username);
            this.handleMainMenu();
        }else {
            warehouseView.printIncorrectCredentialsMessage();
            this.login();

        }


    }

    private void handleMainMenu() {
        warehouseView.printMainMenu();
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.handleStorageRoomMenu();
        }else if(choice.equals("2")){
            this.handleBoxesMenu();
        }else if(choice.equals("3")){
            String confirmation = warehouseView.printLogOutConfirmationMessage();
            if(confirmation.equals("Y"))
            {
                try {
                    warehouseView.printLogOutMessage(warehouseService.getIsLogged().getCostumer().getUsername());
                    warehouseService.logout();

                }catch(IOException e) {
                    warehouseView.printException(e);
                }
            }
            else
            {
                this.handleMainMenu();
            }
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleMainMenu();
        }
    }

    private void handleStorageRoomMenu() {
        warehouseView.printStorageRoomsMenu();
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.listAllStorageRooms();
            this.handleStorageRoomMenu();
        }else if(choice.equals("2")){
            this.listAllStorageRooms();
            this.rentStorageRoom();

        }else if(choice.equals("3")){
            warehouseView.printStorageRoomsRentByCostumer(warehouseService.getLoggedInCostumer());
            warehouseView.printCancelStorageRoomPrompt();
            this.cancelStorageRoomRenting();

        }else if(choice.equals("4")){
            this.listCostumerStorageRooms();
            this.handleStorageRoomMenu();
        }else if(choice.toUpperCase().equals("Q")){
            this.handleMainMenu();
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleStorageRoomMenu();
        }
    }

    public void listAllStorageRooms()
    {
        warehouseService.getAllStorageRooms().forEach(warehouseView::printStorageRoomDetails);
    }
    /*
    private void listAllStorageRooms() {
        warehouseView.printWarehouseStorageRooms(warehouseService.getWarehouse());
    }*/

    private void rentStorageRoom() {
        warehouseView.printRentStorageRoomPrompt();
        String next = warehouseView.getInput().toUpperCase();
        if(next.equals("Q"))
        {
            this.handleStorageRoomMenu();
        }else {
            boolean rentable = warehouseService.isStorageRoomRentable(Long.parseLong(next));
            if(rentable==true)
            {
                warehouseService.rentStorageRoom(Long.parseLong(next));
                warehouseView.printStorageRoomSuccesfullyRented(Long.parseLong(next));
                this.handleStorageRoomMenu();
            }else {
                warehouseView.invalidChoiceMessage();
                this.handleStorageRoomMenu();
            }
        }
    }

    private void cancelStorageRoomRenting() {
        String toCancel = warehouseView.getInput().toUpperCase();
        boolean cancelable = false;
        if(toCancel.equals("Q"))
        {
            this.handleStorageRoomMenu();
        }
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            if(room.getStorageRoomId().equals(Long.parseLong(toCancel)))
            {
                cancelable=true;
            }
        }
        if(cancelable)
        {
            warehouseService.cancelStorageRoomRenting(Long.parseLong(toCancel));
            warehouseView.printStorageRoomSuccesfullyUnRented(Long.parseLong(toCancel));
            this.handleStorageRoomMenu();

        }else {
            warehouseView.invalidChoiceMessage();
            this.handleStorageRoomMenu();
        }
    }

    private void listCostumerStorageRooms() {
        warehouseView.printStorageRoomsRentByCostumer(warehouseService.getLoggedInCostumer());
    }

    private void handleBoxesMenu() {
        warehouseView.printBoxesMenu();;
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.createBox();
            this.handleBoxesMenu();

        }else if(choice.equals("2")){
            this.deleteBox();
            this.handleBoxesMenu();
        }else if(choice.equals("3")){
            this.listBoxes();
            this.handleBoxesMenu();
        }else if(choice.toUpperCase().equals("Q")){
            this.handleMainMenu();
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }
    }

    private void createBox() {
        warehouseView.printCostumerStorageRoomsWithSizes(warehouseService.getLoggedInCostumerFromDatabase());
        warehouseView.printReadStorageRoomPrompt();
        String choiceRoom = warehouseView.getInput();
        boolean isCostumersRoom = false;
        int roomArea=0;
        int boxArea=0;
        int remainingArea=0;
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            if(room.getStorageRoomId().equals(Long.parseLong(choiceRoom)))
            {
                isCostumersRoom=true;
                int x = room.getSize().getX();
                int y = room.getSize().getY();
                roomArea=x*y;
            }
        }
        if(isCostumersRoom)
        {
            Box newBox = warehouseView.readBox();
            Costumer costumer = warehouseService.getLoggedInCostumer();
            StorageRoom storageRoom = warehouseService.getStorageRoomByIs(Long.parseLong(choiceRoom));
            newBox.setStorageRoomId(Long.parseLong(choiceRoom));
            newBox.setCostumer(costumer);
            newBox.setStorageRoom(storageRoom);
            int bx = newBox.getSize().getX();
            int by = newBox.getSize().getY();
            boxArea = bx*by;
            remainingArea = roomArea-boxArea;
            if(remainingArea>0 || remainingArea==0) {
                warehouseService.storeRoom(newBox, Long.parseLong(choiceRoom));
                warehouseView.printNewBoxIsRented(newBox.getBoxId(), Long.parseLong(choiceRoom));
            }else{
                warehouseView.notEnoughRoomMessage();

            }
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }

    }

    private void deleteBox() {
        //warehouseView.printCostumerStorageRoomsWithSizes(warehouseService.getLoggedInCostumer());
        String choiceBox = warehouseView.selectBoxToRemove(warehouseService.getLoggedInCostumer());
        if(choiceBox.equals("Q"))
        {
            this.handleBoxesMenu();
        }
        boolean isCostumersBox = false;
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            for(int i = 0; i < room.getBoxes().size(); i++) {
                Box b = room.getBoxes().get(i);
                if(b.getBoxId().equals(Long.parseLong(choiceBox)))
                {
                    warehouseService.removeBox(Long.parseLong(choiceBox));
                    isCostumersBox = true;
                    warehouseView.deletedBoxMessage(Long.parseLong(choiceBox));
                }
            }
        }
        if(!isCostumersBox)
        {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }

    }

    private void listBoxes() {
        warehouseView.printCostumerBoxes(warehouseService.getLoggedInCostumer());
    }

}
