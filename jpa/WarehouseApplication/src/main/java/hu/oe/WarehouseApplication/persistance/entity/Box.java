package hu.oe.WarehouseApplication.persistance.entity;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.Size;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Box {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long boxId;

    @Embedded
    private Size size;

    @ManyToOne
    @JoinColumn(name="storageRoomId", insertable = false, updatable = false)
    private StorageRoom storageRoom;

    @ManyToOne
    @JoinColumn(name="costumerId")
    private Costumer costumer;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass= Material.class)
    @Enumerated(EnumType.STRING)
    private List<Material> materials;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass= Category.class)
    @Enumerated(EnumType.STRING)
    private List<Category> categories;

    public Box()
    {
        storageRoom = new StorageRoom();
        costumer = new Costumer();
        materials = new ArrayList<Material>();
        categories = new ArrayList<Category>();
    }

    public Long getBoxId() {
        return boxId;
    }
    public void setBoxId(Long boxId) {
        this.boxId = boxId;
    }
    public Size getSize() {
        return size;
    }
    public void setSize(Size size) {
        this.size = size;
    }
    public void setSize2(int x, int y) {
        Size s = new Size();
        s.setX(x);
        s.setY(y);
        this.size = s;
    }

    public List<Material> getMaterials() {
        return materials;
    }
    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }
    public List<Category> getCategories() {
        return categories;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void addMaterials(Material material) {
        this.materials.add(material);
    }

    public void addCategories(Category category)
    {
        this.categories.add(category);
    }

    public StorageRoom getStorageRoom() {
        return storageRoom;
    }

    public void setStorageRoom(StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
    }

    public Long getStorageRoomId() {
        return storageRoom.getStorageRoomId();
    }

    public void setStorageRoomId(Long storageRoomId) {
        this.storageRoom.setStorageRoomId(storageRoomId);
    }


    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }

    public Long getCostumerId() {
        return costumer.getCostumerId();
    }

    public void setCostumerId(Long costumerId) {
        this.costumer.setCostumerId(costumerId);
    }


}
