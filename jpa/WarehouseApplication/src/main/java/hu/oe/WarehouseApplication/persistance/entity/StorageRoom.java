package hu.oe.WarehouseApplication.persistance.entity;
import hu.oe.WarehouseApplication.persistance.Size;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StorageRoom {
    @Id
    @GeneratedValue
    private Long storageRoomId;
    private boolean isFree;

    @ManyToOne
    @JoinColumn(name="costumerId", nullable = true)
    private Costumer costumer;

    @ElementCollection
    @OneToMany(mappedBy="storageRoom", fetch = FetchType.EAGER)
    private List<Box> boxes;
    @Embedded
    private Size size;

    public StorageRoom()
    {
        costumer = new Costumer();
        costumer.setCostumerId(0L);
        isFree = true;
        boxes=new ArrayList<Box>();
    }


    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public void setSize2(int x, int y) {
        Size s = new Size();
        s.setX(x);
        s.setY(y);
        this.size = s;
    }

    public Long getStorageRoomId() {
        return storageRoomId;
    }

    public void setStorageRoomId(Long storageRoomId) {
        this.storageRoomId = storageRoomId;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean isFree) {
        this.isFree = isFree;
    }


    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }

    public Long getCostumerId() {
        return costumer.getCostumerId();
    }

    public void setCostumerId(Long costumerId) {
        this.costumer.setCostumerId(costumerId);
    }

    public List<Box> getBoxes() {
        return boxes;
    }


    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public void addBox(Box box) {
        this.boxes.add(box);
    }

    public void removeBox(Box box) {
        this.boxes.remove(boxes.indexOf(box));
    }
}
