package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;

import java.io.IOException;

public interface Service {


    public boolean authenticate (String username, String password);

    public boolean isLoggedIn();

    public Costumer getLoggedInCostumer();

    public void logout() throws IOException;

    public void rentStorageRoom(Long storageRoomId);

    public void cancelStorageRoomRenting(Long storageRoomId);

    public void storeRoom(Box box, Long storageRoomId);

    public void removeBox(Long boxId);

    public boolean isStorageRoomRentable(Long id);
}
