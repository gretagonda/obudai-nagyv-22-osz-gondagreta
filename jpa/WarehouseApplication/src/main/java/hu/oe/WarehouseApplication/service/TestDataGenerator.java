package hu.oe.WarehouseApplication.service;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.Costumer;
import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.persistance.repository.BoxRepository;
import hu.oe.WarehouseApplication.persistance.repository.CostumerRepository;
import hu.oe.WarehouseApplication.persistance.repository.StorageRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class TestDataGenerator {
    @Autowired
    BoxRepository boxRepository;
    @Autowired
    CostumerRepository costumerRepository;
    @Autowired
    StorageRoomRepository storageRoomRepository;

    @Transactional
    public void createTestData(){
        Costumer costumer1 = createCostumer("gipszjakab", "password");
        costumerRepository.save(costumer1);
        StorageRoom storageRoom1 = createStorageRoom(costumer1, 10,10);
        storageRoomRepository.save(storageRoom1);
        StorageRoom storageRoom2 = createStorageRoom(costumer1, 8,15);
        StorageRoom storageRoom3 = createStorageRoom2( 8,8);
        storageRoomRepository.save(storageRoom2);
        storageRoomRepository.save(storageRoom3);
        Box box1 = createBox(2,2,storageRoom1, costumer1, "light", "paper");
        boxRepository.save(box1);
        Box box2 = createBox(3,3,storageRoom2, costumer1, "heavy", "glass");
        boxRepository.save(box2);


    }


    private Box createBox(int x, int y, StorageRoom storageRoom, Costumer costumer, String cat, String mat)
    {
        Box box = new Box();
        Category category = Category.valueOf(cat.toUpperCase());
        box.addCategories(category);
        Material material = Material.valueOf(mat.toUpperCase());
        box.addMaterials(material);
        box.setSize2(x,y);
        box.setStorageRoom(storageRoom);
        box.setCostumer(costumer);
        return box;
    }

    private Costumer createCostumer(String username, String password)
    {
        Costumer costumer = new Costumer();
        costumer.setUsername(username);
        costumer.setPassword(password);

        return costumer;
    }

    private StorageRoom createStorageRoom(Costumer costumer, int x, int y) {
        StorageRoom storageRoom = new StorageRoom();
        storageRoom.setCostumer(costumer);
        storageRoom.setSize2(x, y);
        storageRoom.setFree(costumer == null);

        return storageRoom;
    }

    private StorageRoom createStorageRoom2(int x, int y) {
        StorageRoom storageRoom = new StorageRoom();
        storageRoom.setSize2(x, y);

        return storageRoom;
    }
}
