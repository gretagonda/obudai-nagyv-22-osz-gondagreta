package hu.oe.warehouse.persistance;

import hu.oe.warehouse.domain.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

public class Data {
    public Data()
    {

    }

    public List<StorageRoom> readStorages() throws FileNotFoundException {
        //storages.txt

        List<StorageRoom> storageRooms = new ArrayList<StorageRoom>();
        try {
            File storageFile = new File("storages.txt");
            Scanner reader = new Scanner(storageFile);
            while (reader.hasNextLine()) {
                StorageRoom storageRoom = new StorageRoom();
                String data = reader.nextLine();
                String[] dataArray = data.split(" ");
                storageRoom.setId(Long.parseLong(dataArray[0]));
                String[] sizeArray = dataArray[1].split("x");
                Size size = new Size();
                size.setX(parseInt(sizeArray[0]));
                size.setY(parseInt(sizeArray[1]));
                storageRoom.setSize(size);
                storageRooms.add(storageRoom);
            }
            reader.close();
        }catch(FileNotFoundException e)
        {
            throw new FileNotFoundException("storage.txt not exists or data format is invalid");
        }
        return storageRooms;
    }

    public List<Costumer> readCostumers() throws FileNotFoundException {
        //costumers.txt
        List<Costumer> costumers = new ArrayList<Costumer>();
        try {
            File costumerFile = new File("costumers.txt");
            Scanner reader = new Scanner(costumerFile);
            while (reader.hasNextLine()) {
                Costumer costumer = new Costumer();
                String data = reader.nextLine();
                String[] dataArray = data.split(" ");
                costumer.setId(Long.parseLong(dataArray[0]));
                costumer.setUsername(dataArray[1]);
                costumer.setPassword(dataArray[2]);
                costumers.add(costumer);
            }
            reader.close();
        }catch(FileNotFoundException e)
        {
            throw new FileNotFoundException("costumer.txt not exists or data format is invalid");
        }
        return costumers;

    }

    public List<StorageRoom> readWarehouse() throws FileNotFoundException {
        //warehouse.txt
        List<StorageRoom> storageRooms = new ArrayList<StorageRoom>();


        try {
            File storageFile = new File("warehouse.txt");
            Scanner reader = new Scanner(storageFile);
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                String[] warehouseArray = data.split("\\|");
                StorageRoom storageRoom = new StorageRoom();
                for (int i = 0; i < warehouseArray.length; i++)
                {
                    String[] dataArray = warehouseArray[i].split(" ");
                    storageRoom.setId(Long.parseLong(dataArray[0]));
                    storageRoom.setOwnerId(Long.parseLong(dataArray[1]));
                    storageRoom.setFree(false);
                    List<Box> boxes = this.readBoxes();
                    for (int j = 2; j < dataArray.length; j++)
                    {
                        for(Box box : boxes)
                        {
                            if(box.getId().equals(Long.parseLong(dataArray[j])))
                            {
                                box.setStorageRoomId(Long.parseLong(dataArray[0]));
                                storageRoom.addBox(box);
                            }
                        }
                    }
                }
                storageRooms.add(storageRoom);
            }
            reader.close();
        }catch(FileNotFoundException e)
        {
            throw new FileNotFoundException("warehouse.txt not exists or data format is invalid");
        }
        return storageRooms;
    }

    public List<Box> readBoxes() throws FileNotFoundException {
        //boxes.txt
        List<Box> boxes = new ArrayList<Box>();


        try {
            File boxesFile = new File("boxes.txt");
            Scanner reader = new Scanner(boxesFile);
            while (reader.hasNextLine()) {
                String data = reader.nextLine();
                String[] boxesArray = data.split(" ");
                Box box = new Box();

                box.setId(Long.parseLong(boxesArray[0]));
                box.setCostumerId(Long.parseLong(boxesArray[1]));

                String[] boxesArray1b = boxesArray[2].split("x");
                Size size = new Size();
                size.setX(parseInt(boxesArray1b[0]));
                size.setY(parseInt(boxesArray1b[1]));
                box.setSize(size);

                String[] boxesMaterial = boxesArray[3].split("\\|");
                for(int i = 0; i < boxesMaterial.length; i++ ) {

                    Material material = Material.valueOf(boxesMaterial[i]);
                    box.addMaterials(material);

                }

                String[] boxesCategory = boxesArray[4].split("\\|");
                for(int i = 0; i < boxesCategory.length; i++ ) {

                    Category category = Category.valueOf(boxesCategory[i]);
                    box.addCategories(category);

                }
                boxes.add(box);
            }
            reader.close();
        }catch(FileNotFoundException e)
        {
            throw new FileNotFoundException("boxes.txt not exists or data format is invalid");
        }
        return boxes;
    }

    public void writeWarehouse(List<StorageRoom> storageRooms) throws IOException {
        //warehouse.txt
        BufferedWriter writer = new BufferedWriter(new FileWriter("warehouse.txt"));

        for(StorageRoom room:storageRooms)
        {
            if(!(room.getOwnerId().equals(0L)))
            {
                String line = room.getId() + " " + room.getOwnerId();
                int num = 0;
                for(Box box: room.getBoxes())
                {
                    if(num==0) {
                        line = line + " " + box.getId();
                    }else{
                        line = line + "|" + box.getId();
                    }
                    num+=1;
                }
                line = line + "\n";
                writer.write(line);
            }
        }

        writer.close();

    }

    public void writeBoxes(List<Box> cs) throws IOException{
        //boxes.txt
        BufferedWriter writer = new BufferedWriter(new FileWriter("boxes.txt"));

        for(Box box:cs)
        {
            String line = box.getId()+" ";
            line = line+box.getCostumerId()+" ";
            line=line+box.getSize().getX()+"x"+box.getSize().getY()+" ";
            int counter = 0;
            for(Material mat:box.getMaterials())
            {
                if(counter == 0)
                {
                    line = line+mat;
                }
                else
                {
                    line = line+"|"+mat;

                }

                counter+=1;
            }
            line = line + " ";
            counter = 0;
            int max = box.getCategories().size()-1;
            for(Category cat:box.getCategories())
            {
                if(counter == max)
                {
                    line = line+cat;
                }
                else {
                    line = line + cat + "|";
                }

                counter+=1;
            }
            line = line + "\n";
            writer.write(line);
        }
        writer.close();
    }
}
