package hu.oe.warehouse.service;

import hu.oe.warehouse.domain.Costumer;

public class IsLogged {
    private boolean isLoggedIn;
    private Costumer costumer;
    public IsLogged()
    {
        isLoggedIn = false;
        setCostumer(new Costumer());
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean isLoggedIn) {
        this.isLoggedIn = isLoggedIn;
    }

    public Costumer getCostumer() {
        return costumer;
    }

    public void setCostumer(Costumer costumer) {
        this.costumer = costumer;
    }
}
