package hu.oe.warehouse.app;

import hu.oe.warehouse.domain.Warehouse;
import hu.oe.warehouse.service.WarehouseService;
import hu.oe.warehouse.view.WarehouseView;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringApp {

    public static void main(String[] args) {
        SpringApplication.run(SpringApp.class, args);
        Warehouse warehouse = new Warehouse();
        WarehouseService warehousService = new WarehouseService(warehouse);
        WarehouseView warehousView = new WarehouseView(warehouse);

        App app = new App(warehousView, warehousService, warehouse);
        app.start();

    }

}
