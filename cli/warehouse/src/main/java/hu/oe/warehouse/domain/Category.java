package hu.oe.warehouse.domain;

public enum Category {
    FLAMMABLE,
    LIGHT,
    HEAVY,
    FRAGILE
}
