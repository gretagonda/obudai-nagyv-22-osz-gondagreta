package hu.oe.warehouse.domain;
import java.util.ArrayList;
import java.util.List;


public class Warehouse {

    private List<StorageRoom> storageRooms;
    private List<Costumer> costumers;
    private List<Box> boxes;

    public Warehouse()
    {
        storageRooms = new ArrayList<StorageRoom>();
        costumers = new ArrayList<Costumer>();
        boxes =new ArrayList<Box>();

    }

    public List<StorageRoom> getStorageRooms() {
        return storageRooms;
    }

    public void setStorageRooms(List<StorageRoom> storageRooms) {
        this.storageRooms = storageRooms;
    }

    public List<Costumer> getCostumers() {
        return costumers;
    }

    public void setCostumers(List<Costumer> costumers) {
        this.costumers = costumers;
    }

    public List<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }
}
