package hu.oe.warehouse.domain;

public enum Material {
    GLASS,
    TIMBER,
    METAL,
    PAPER
}
