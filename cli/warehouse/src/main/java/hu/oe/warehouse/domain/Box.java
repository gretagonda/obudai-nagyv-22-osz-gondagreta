package hu.oe.warehouse.domain;

import java.util.ArrayList;
import java.util.List;

public class Box {
    private static int counter = 0;
    private Long id;
    private Size size;
    private Long storageRoomId;
    private Long costumerId;
    private List<Material> materials;
    private List<Category> categories;

    public Box()
    {
        materials = new ArrayList<Material>();
        categories = new ArrayList<Category>();
    }

    public static int getCounter() {
        return counter;
    }

    public static void setCounter(int counterPlus) {
        Box.counter = counter+counterPlus;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Size getSize() {
        return size;
    }
    public void setSize(Size size) {
        this.size = size;
    }
    public Long getStorageRoomId() {
        return storageRoomId;
    }
    public void setStorageRoomId(Long storageRoomId) {
        this.storageRoomId = storageRoomId;
    }
    public Long getCostumerId() {
        return costumerId;
    }
    public void setCostumerId(Long costumerId) {
        this.costumerId = costumerId;
    }
    public List<Material> getMaterials() {
        return materials;
    }
    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }
    public List<Category> getCategories() {
        return categories;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void addMaterials(Material material) {
        this.materials.add(material);
    }

    public void addCategories(Category category)
    {
        this.categories.add(category);
    }
}
