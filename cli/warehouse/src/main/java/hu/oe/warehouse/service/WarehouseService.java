package hu.oe.warehouse.service;

import hu.oe.warehouse.domain.Box;
import hu.oe.warehouse.domain.Costumer;
import hu.oe.warehouse.domain.StorageRoom;
import hu.oe.warehouse.domain.Warehouse;
import hu.oe.warehouse.persistance.Data;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class WarehouseService implements Service{
    private Warehouse warehouse;

    public IsLogged getIsLogged() {
        return isLogged;
    }

    public void setIsLogged(IsLogged isLogged) {
        this.isLogged = isLogged;
    }

    private IsLogged isLogged;
    private Data data;

    public WarehouseService(Warehouse warehouse)
    {
        this.warehouse = warehouse;
        this.isLogged = new IsLogged();
        this.data = new Data();

    }


    @Override
    public void saveData() throws IOException {
        data.writeWarehouse(warehouse.getStorageRooms());
        data.writeBoxes(warehouse.getBoxes());

    }

    @Override
    public void initializeData() throws FileNotFoundException {
        //Data data = new Data();

        List<StorageRoom> storagesAll = data.readStorages();
        List<StorageRoom> storagesUsed = data.readWarehouse();
        for(StorageRoom room :storagesAll)
        {
            StorageRoom storage = null;
            for(StorageRoom usedroom : storagesUsed)
            {
                if(usedroom.getId().equals(room.getId()))
                {
                    storage=usedroom;
                }
            }

            if(storage!=(null))
            {
                room.setOwnerId(storage.getOwnerId());
                room.setBoxes(storage.getBoxes());
                room.setFree(false);
            }

        }
        warehouse.setStorageRooms(storagesAll);

        List<Costumer> costumers = data.readCostumers();
        for(Costumer costumer :costumers)
        {
            for(StorageRoom storage :storagesAll)
            {
                if(costumer.getId().equals(storage.getOwnerId()))
                {
                    costumer.addStorageRoom(storage);
                }
            }

        }
        warehouse.setCostumers(costumers);

        List<Box> boxes = data.readBoxes();
        int c = boxes.size();
        Box.setCounter(c);
        warehouse.setBoxes(boxes);


    }

    @Override
    public boolean authenticate(String username, String password) {

        for(Costumer costumer : warehouse.getCostumers())
        {
            if(username.equals(costumer.getUsername()) && password.equals(costumer.getPassword()))
            {
                isLogged.setLoggedIn(true);
                isLogged.setCostumer(costumer);
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isLoggedIn() {
        if(isLogged.isLoggedIn()==true)
        {
            return true;
        }
        return false;
    }

    @Override
    public Costumer getLoggedInCostumer() {
        if(isLogged.isLoggedIn()==true)
        {
            return isLogged.getCostumer();
        }
        return null;
    }

    @Override
    public void logout() throws IOException {
        this.saveData();
        isLogged.setLoggedIn(false);
    }

    @Override
    public Warehouse getWarehouse() {
        return warehouse;
    }

    @Override
    public StorageRoom getStorageRoom(Long storageRoomId) {

        return warehouse.getStorageRooms().stream()
                .filter(s -> s.getId().equals(storageRoomId))
                .findAny()
                .orElse(null);
    }

    @Override
    public void rentStorageRoom(Long storageRoomId) {
        for(int i = 0; i < warehouse.getStorageRooms().size(); i++)
        {
            if(warehouse.getStorageRooms().get(i).getId().equals(storageRoomId))
            {
                warehouse.getStorageRooms().get(i).setFree(false);
                warehouse.getStorageRooms().get(i).setOwnerId(isLogged.getCostumer().getId());
                isLogged.getCostumer().addStorageRoom(warehouse.getStorageRooms().get(i));
            }
        }
    }

    @Override
    public void cancelStorageRoomRenting(Long storageRoomId) {
        for(int i = 0; i < warehouse.getStorageRooms().size(); i++)
        {
            if(warehouse.getStorageRooms().get(i).getId().equals(storageRoomId))
            {
                warehouse.getStorageRooms().get(i).setFree(true);
                warehouse.getStorageRooms().get(i).setOwnerId(null);
                isLogged.getCostumer().removeStorageRoom(warehouse.getStorageRooms().get(i));
            }
        }
    }

    @Override
    public void storeRoom(Box box, Long storageRoomId) {
        for(int i = 0; i < warehouse.getStorageRooms().size(); i++)
        {
            if(warehouse.getStorageRooms().get(i).getId().equals(storageRoomId))
            {
                //warehouse.getStorageRooms().get(i).addBox(box);
                for(int j = 0; j < isLogged.getCostumer().getStorageRooms().size(); j++)
                {
                    if(isLogged.getCostumer().getStorageRooms().get(j).getId().equals(storageRoomId))
                    {
                        isLogged.getCostumer().getStorageRooms().get(j).addBox(box);
                    }
                }

            }
        }
    }

    @Override
    public void removeBox(Long boxId) {
        /*for(int i = 0; i < warehouse.getStorageRooms().size(); i++)
        {
            for(Box box : warehouse.getStorageRooms().get(i).getBoxes())
            {
                if(box.getId().equals(boxId))
                {
                    warehouse.getStorageRooms().get(i).removeBox(box);
                }
            }
        }*/
        for(int i = 0; i < isLogged.getCostumer().getStorageRooms().size(); i++)
        {
            for(int j = 0; j < isLogged.getCostumer().getStorageRooms().get(i).getBoxes().size(); j++)
            {
                Box b = isLogged.getCostumer().getStorageRooms().get(i).getBoxes().get(j);
                if(b.getId().equals(boxId))
                {
                    isLogged.getCostumer().getStorageRooms().get(i).removeBox(b);
                    Box.setCounter(-1);
                }
            }
        }

    }

    @Override
    public boolean isStorageRoomRentable(Long id) {
        for(StorageRoom room: warehouse.getStorageRooms())
        {
            if(room.getId().equals(id))
            {
                return room.isFree();
            }
        }
        return false;
    }

}
