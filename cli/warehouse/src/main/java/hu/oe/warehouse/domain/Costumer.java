package hu.oe.warehouse.domain;

import java.util.ArrayList;
import java.util.List;

public class Costumer {
    private List<StorageRoom> storageRooms;
    private Long id;
    private String username;
    private String password;

    public Costumer()
    {
        storageRooms = new ArrayList<StorageRoom>();
    }

    public List<StorageRoom> getStorageRooms() {
        return storageRooms;
    }

    public void setStorageRooms(List<StorageRoom> storageRooms) {
        this.storageRooms = storageRooms;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addStorageRoom(StorageRoom storageroom)
    {
        storageRooms.add(storageroom);
    }

    public void removeStorageRoom(StorageRoom storageroom)
    {
        storageRooms.remove(storageroom);
    }

}
