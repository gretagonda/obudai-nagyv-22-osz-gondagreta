package hu.oe.warehouse.service;

import hu.oe.warehouse.domain.Box;
import hu.oe.warehouse.domain.Costumer;
import hu.oe.warehouse.domain.StorageRoom;
import hu.oe.warehouse.domain.Warehouse;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface Service {
    public void saveData() throws IOException;

    public void initializeData() throws FileNotFoundException;

    public boolean authenticate (String username, String password);

    public boolean isLoggedIn();

    public Costumer getLoggedInCostumer();

    public void logout() throws IOException;

    public Warehouse getWarehouse();

    public StorageRoom getStorageRoom(Long storageRoomId);

    public void rentStorageRoom(Long storageRoomId);

    public void cancelStorageRoomRenting(Long storageRoomId);

    public void storeRoom(Box box, Long storageRoomId);

    public void removeBox(Long boxId);

    public boolean isStorageRoomRentable(Long id);
}
