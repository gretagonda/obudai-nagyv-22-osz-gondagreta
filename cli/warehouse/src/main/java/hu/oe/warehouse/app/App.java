package hu.oe.warehouse.app;

import hu.oe.warehouse.domain.Box;
import hu.oe.warehouse.domain.StorageRoom;
import hu.oe.warehouse.domain.Warehouse;
import hu.oe.warehouse.service.WarehouseService;
import hu.oe.warehouse.view.WarehouseView;

import java.io.FileNotFoundException;
import java.io.IOException;

public class App {
    private WarehouseView warehouseView;

    private Warehouse warehouse;

    private WarehouseService warehouseService;
    public App(WarehouseView warehouseView, WarehouseService warehouseService, Warehouse warehouse )
    {
        this.warehouseView = warehouseView;
        this.warehouse = warehouse;
        this.warehouseService = warehouseService;

    }

    public App()
    {

    }

    public void start() {
        warehouseView.printWelcomeMessage();
        try {
            warehouseService.initializeData();
        }catch(FileNotFoundException e)
        {
            warehouseView.printException(e);
        }
        this.login();
    }

    private void login() {
        String username = warehouseView.readUsername();
        String password = warehouseView.readPassword();
        //List<StorageRoom> sr = warehouse.getStorageRooms();
        boolean isAuthenticated = warehouseService.authenticate(username, password);
        if(isAuthenticated == true)
        {
            warehouseView.printAuthenticatedMessage(username);
            this.handleMainMenu();
        }else {
            warehouseView.printIncorrectCredentialsMessage();
            this.login();

        }


    }

    private void handleMainMenu() {
        warehouseView.printMainMenu();
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.handleStorageRoomMenu();
        }else if(choice.equals("2")){
            this.handleBoxesMenu();
        }else if(choice.equals("3")){
            String confirmation = warehouseView.printLogOutConfirmationMessage();
            if(confirmation.equals("Y"))
            {
                try {
                    warehouseView.printLogOutMessage(warehouseService.getIsLogged().getCostumer().getUsername());
                    warehouseService.logout();
                }catch(IOException e) {
                    warehouseView.printException(e);
                }
            }
            else
            {
                this.handleMainMenu();
            }
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleMainMenu();
        }
    }

    private void handleStorageRoomMenu() {
        warehouseView.printStorageRoomsMenu();
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.listAllStorageRooms();
            this.handleStorageRoomMenu();
        }else if(choice.equals("2")){
            this.listAllStorageRooms();
            this.rentStorageRoom();

        }else if(choice.equals("3")){
            warehouseView.printStorageRoomsRentByCostumer(warehouseService.getLoggedInCostumer());
            warehouseView.printCancelStorageRoomPrompt();
            this.cancelStorageRoomRenting();

        }else if(choice.equals("4")){
            this.listCostumerStorageRooms();
            this.handleStorageRoomMenu();
        }else if(choice.toUpperCase().equals("Q")){
            this.handleMainMenu();
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleStorageRoomMenu();
        }
    }

    private void listAllStorageRooms() {
        warehouseView.printWarehouseStorageRooms();
    }

    private void rentStorageRoom() {
        warehouseView.printRentStorageRoomPrompt();
        String next = warehouseView.getInput().toUpperCase();
        if(next.equals("Q"))
        {
            this.handleStorageRoomMenu();
        }else {
            boolean rentable = warehouseService.isStorageRoomRentable(Long.parseLong(next));
            if(rentable==true)
            {
                warehouseService.rentStorageRoom(Long.parseLong(next));
                warehouseView.printStorageRoomSuccesfullyRented(Long.parseLong(next));
                this.handleStorageRoomMenu();
            }else {
                warehouseView.invalidChoiceMessage();
                this.handleStorageRoomMenu();
            }
        }
    }

    private void cancelStorageRoomRenting() {
        String toCancel = warehouseView.getInput().toUpperCase();
        boolean cancelable = false;
        if(toCancel.equals("Q"))
        {
            this.handleStorageRoomMenu();
        }
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            if(room.getId().equals(Long.parseLong(toCancel)))
            {
                cancelable=true;
            }
        }
        if(cancelable)
        {
            warehouseService.cancelStorageRoomRenting(Long.parseLong(toCancel));
            warehouseView.printStorageRoomSuccesfullyUnRented(Long.parseLong(toCancel));
            this.handleStorageRoomMenu();

        }else {
            warehouseView.invalidChoiceMessage();
            this.handleStorageRoomMenu();
        }
    }

    private void listCostumerStorageRooms() {
        warehouseView.printStorageRoomsRentByCostumer(warehouseService.getLoggedInCostumer());
    }

    private void handleBoxesMenu() {
        warehouseView.printBoxesMenu();;
        String choice = warehouseView.getInput();
        if(choice.equals("1"))
        {
            this.createBox();
            this.handleBoxesMenu();

        }else if(choice.equals("2")){
            this.deleteBox();
            this.handleBoxesMenu();
        }else if(choice.equals("3")){
            this.listBoxes();
            this.handleBoxesMenu();
        }else if(choice.toUpperCase().equals("Q")){
            this.handleMainMenu();
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }
    }

    private void createBox() {
        warehouseView.printCostumerStorageRoomsWithSizes(warehouseService.getLoggedInCostumer());
        warehouseView.printReadStorageRoomPrompt();
        String choiceRoom = warehouseView.getInput();
        boolean isCostumersRoom = false;
        int roomArea=0;
        int boxArea=0;
        int remainingArea=0;
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            if(room.getId().equals(Long.parseLong(choiceRoom)))
            {
                isCostumersRoom=true;
                int x = room.getSize().getX();
                int y = room.getSize().getY();
                roomArea=x*y;
            }
        }
        if(isCostumersRoom)
        {
            Box newBox = warehouseView.readBox(warehouseService.getLoggedInCostumer().getId(),Long.parseLong(choiceRoom));
            newBox.setStorageRoomId(Long.parseLong(choiceRoom));
            int bx = newBox.getSize().getX();
            int by = newBox.getSize().getY();
            boxArea = bx*by;
            remainingArea = roomArea-boxArea;
            if(remainingArea>0 || remainingArea==0) {
                warehouseService.storeRoom(newBox, Long.parseLong(choiceRoom));
                warehouse.getBoxes().add(newBox);
                warehouseView.printNewBoxIsRented(newBox.getId(), Long.parseLong(choiceRoom));
            }else{
                warehouseView.notEnoughRoomMessage();

            }
        }else {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }

    }

    private void deleteBox() {
        //warehouseView.printCostumerStorageRoomsWithSizes(warehouseService.getLoggedInCostumer());
        String choiceBox = warehouseView.selectBoxToRemove(warehouseService.getLoggedInCostumer());
        if(choiceBox.equals("Q"))
        {
            this.handleBoxesMenu();
        }
        boolean isCostumersBox = false;
        for(StorageRoom room: warehouseService.getLoggedInCostumer().getStorageRooms())
        {
            for(int i = 0; i < room.getBoxes().size(); i++) {
                Box b = room.getBoxes().get(i);
                if(b.getId().equals(Long.parseLong(choiceBox)))
                {
                    warehouseService.removeBox(Long.parseLong(choiceBox));
                    isCostumersBox = true;
                }
            }
        }
        if(!isCostumersBox)
        {
            warehouseView.invalidChoiceMessage();
            this.handleBoxesMenu();
        }

    }

    private void listBoxes() {
        warehouseView.printCostumerBoxes(warehouseService.getLoggedInCostumer());
    }
}
