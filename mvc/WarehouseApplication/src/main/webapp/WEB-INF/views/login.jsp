<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/login.css">
</head>
    <h1>Welcome to Warehouse Application</h1>
<body>
    <c:if test="${param.error != null}">
    <div>
        Invalid username and password.
    </div>
    </c:if>
    <c:if test="${param.logout != null}">
    <div>
        You have been logged out.
    </div>
    </c:if>
    <form:form action="login" method='Post'>
    <div class="row">
        <div class="col-25">
          <label>User Name: </label>
        </div>
        <div class="col-75">
          <input type="text" name="username" placeholder="Your username">
        </div>
      </div>
     <div class="row">
         <div class="col-25">
            <label>Password: </label>
         </div>
         <div class="col-75">
            <input type="password" name="password" placeholder="Your password">
         </div>
      </div>
      <div class="row">
         <input type="submit" value="Login">
      </div>
    </form:form>

</body>
</html>
