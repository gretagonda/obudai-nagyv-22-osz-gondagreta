<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
<div class = "row">
    <form:form action="logout">
        <input type="submit" value="Logout">
    </form:form>
    </div>
    <br><br>
    <div class = "row">
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>


    <div class = "row">
    <h2>My boxes</h2>
    </div>


    <table>
    <tr>
        <th>Id</th>
        <th>Storage room id</th>
        <th>Size</th>
        <th>Materials</th>
        <th>Categories</th>
    </tr>
    <c:forEach items="${boxes}" var="box">
    <tr>
        <td>${box.id}</td>
        <td>${box.storageroom_id}</td>
        <td>${box.size}</td>
        <td>${box.materials}</td>
        <td>${box.categories}</td>
        <td>

        <form action="/removebox" method="post">
                <input type="text" style="display:none" id ="boxId" name="boxId" value="${box.id}">
                <input type="submit" value="Remove box">
        </form>

        </td>
    </tr>
    </c:forEach>
    </table>
    <br><br>


    <div class = "row">
    <p>You do not rent any storage room at the moment!</p>
    </div>


</body>
</html>
