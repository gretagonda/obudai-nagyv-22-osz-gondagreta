<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
    <form:form action="logout">
        <input type="submit" value="Logout">
    </form:form>
    <br><br>
    <div>
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>

    <h1>Error page</h1>
        <div class="message">
            <c:if test="${not empty errorCode}">
                ${errorCode} :$(errorMessage)
            </c:if>
                <c:if test="${empty errorCode}">
                    System error.
                </c:if>
        <div>


</body>
</html>
