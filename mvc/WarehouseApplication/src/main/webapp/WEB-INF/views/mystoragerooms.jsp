<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
<div class = "row">
    <form:form action="logout">
        <input type="submit" value="Logout">
    </form:form>
    </div>
    <br><br>
    <div class = "row">
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>


    <div class = "row">
    <h2>My storage rooms</h2>
    </div>

    <table>
    <tr>
        <th>Id</th>
        <th>Size</th>
        <th>Owner</th>
        <th>Is free</th>
        <th>Number of boxes</th>
    </tr>
    <c:forEach items="${mystoragerooms}" var="storageroom">
    <tr>
        <td>${storageroom.storageRoom_id}</td>
        <td>${storageroom.size}</td>
        <td>${storageroom.owner}</td>
        <td>${storageroom.isFree}</td>
        <td>${storageroom.numBoxes}</td>
        <td>

        <form action="/cancelstorageroom" method="post">
                <input type="text" style="display:none" id ="storageRoomId" name="storageRoomId" value="${storageroom.storageRoom_id}">
                <input type="submit" value="Cancel renting">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

        </form>

        </td>
    </tr>
    </c:forEach>
    </table>
    <br><br>
</body>
</html>
