<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/storageroom.css">
</head>
<body>
<div class = "row">
    <form:form action="logout">
        <input type="submit" value="Logout">
    </form:form>
    </div>
    <br><br>
    <div class = "row">
    <a href="storagerooms">
        <button>Storage rooms</button>
    </a>
    <a href="mystoragerooms">
        <button>My Storage rooms</button>
    </a>
    <a href="boxes">
    <button>My boxes</button>
    </a>
    </div>
    <br><br>


    <div class = "row">
    <h2>My boxes</h2>
    </div>




    <table>
    <tr>
        <th>Id</th>
        <th>Storage room id</th>
        <th>Size</th>
        <th>Materials</th>
        <th>Categories</th>
    </tr>
    <c:forEach items="${boxes}" var="box">
    <tr>
        <td>${box.id}</td>
        <td>${box.storageroom_id}</td>
        <td>${box.size}</td>
        <td>${box.materials}</td>
        <td>${box.categories}</td>
        <td>

        <form action="/removebox" method="post">
                <input type="text" style="display:none" id ="boxId" name="boxId" value="${box.id}">
                <input type="submit" value="Remove box">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </form>

        </td>
    </tr>
    </c:forEach>
    </table>
    <br><br>


    <form:form modelAttribute="addboxform" action="/boxes" method="post">
        <div class="row">
        <div class="col-25">
            <form:label path="storageroomid">Storage Room Id</form:label>
        </div>
        <div class="col-75">
            <form:errors cssClass="message" path="storageroomid"/>
            <form:select path="storageroomid">
                <form:option value="" label="-- select storage room by id"/>
                <form:options items="${storagerooms}"/>
            </form:select>
        </div>
        </div>
        <div class="row">
        <div class="col-25">
            <form:label path="size">Size</form:label>
        </div>
        <div class="col-75">
            <form:errors cssClass="message" path="size"/>
            <form:input path="size"/>
         </div>
        </div>
        <div class="row">
        <div class="col-25">
            <form:label path="materials">Materials</form:label>
        </div>
        <div class="col-75">
            <form:errors cssClass="message" path="materials"/>
            <form:select size="4" path="materials">
                <form:option value="" label=""/>
                <form:options items="${materials}"/>
            </form:select>
        </div>
        </div>
        <div class="row">
        <div class="col-25">
            <form:label path="categories">Categories</form:label>
        </div>
        <div class="col-75">
            <form:errors cssClass="categories" path="size"/>
            <form:select size="4" path="categories">
                <form:option value="" label=""/>
                <form:options items="${categories}"/>
            </form:select>
        </div>
        </div>
        <div class="row">
            <input type="submit" value="Save"/>
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </form:form>


</body>
</html>
