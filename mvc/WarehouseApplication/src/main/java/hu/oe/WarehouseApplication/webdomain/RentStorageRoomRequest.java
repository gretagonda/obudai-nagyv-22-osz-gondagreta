package hu.oe.WarehouseApplication.webdomain;

import hu.oe.WarehouseApplication.validation.StorageRoomIdConstraint;

public class RentStorageRoomRequest {

    @StorageRoomIdConstraint
    private String storageRoomId;

    public String getStorageRoomId() {
        return storageRoomId;
    }

    public void setStorageRoomId(String storageRoomId) {
        this.storageRoomId = storageRoomId;
    }
}
