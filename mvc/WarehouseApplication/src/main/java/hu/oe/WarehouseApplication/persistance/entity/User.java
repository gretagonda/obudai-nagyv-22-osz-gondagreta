package hu.oe.WarehouseApplication.persistance.entity;

import hu.oe.WarehouseApplication.persistance.UserType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class User {

    @OneToMany(mappedBy="user", fetch = FetchType.EAGER)
    @ElementCollection
    private List<hu.oe.WarehouseApplication.persistance.entity.StorageRoom> storageRooms;
    @Id
    @GeneratedValue
    private Long userId;

    private String username;

    private String password;

    @Enumerated(EnumType.STRING)
    private UserType userType;

    public User()
    {

        storageRooms = new ArrayList<>();
        userType=UserType.COSTUMER;
    }

    public List<hu.oe.WarehouseApplication.persistance.entity.StorageRoom> getStorageRooms() {
        return storageRooms;
    }

    public void setStorageRooms(List<hu.oe.WarehouseApplication.persistance.entity.StorageRoom> storageRooms) {
        this.storageRooms = storageRooms;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = new BCryptPasswordEncoder().encode(password);
    }

    public void addStorageRoom(hu.oe.WarehouseApplication.persistance.entity.StorageRoom storageroom)
    {
        storageRooms.add(storageroom);
    }

    public void removeStorageRoom(hu.oe.WarehouseApplication.persistance.entity.StorageRoom storageroom)
    {
        storageRooms.remove(storageroom);
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }


    public void removeStorageRoomById(Long storageroom)
    {
        hu.oe.WarehouseApplication.persistance.entity.StorageRoom sr = new hu.oe.WarehouseApplication.persistance.entity.StorageRoom();
        for(hu.oe.WarehouseApplication.persistance.entity.StorageRoom room : storageRooms)
        {
            if(room.getStorageRoomId().equals(storageroom))
            {
                sr = room;
            }
        }
        storageRooms.remove(sr);
    }

}
