package hu.oe.WarehouseApplication.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class BoxSizeFormatValidator implements ConstraintValidator<BoxSizeConstraint, String>{

    @Override
    public boolean isValid(String sizeField, ConstraintValidatorContext ctx)
    {
        boolean valid = true;
        if(sizeField.equals(""))
        {
            valid=false;
        }
        String[] xy = new String[2];

        if(!sizeField.contains("x"))
        {
            valid=false;
        }else{
            xy=sizeField.split("x");
            if(!xy[0].matches("[0-9]"))
            {
                valid=false;
            }
            if(!xy[1].matches("[0-9]"))
            {
                valid=false;
            }
        }


        return valid;
    }
}
