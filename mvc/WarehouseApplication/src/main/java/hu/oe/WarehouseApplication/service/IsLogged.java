package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.persistance.entity.User;
import org.springframework.stereotype.Component;

@Component
public class IsLogged {
    private User user;
    public IsLogged()
    {
        setUser(new User());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getLoggedInUserId(){
        return user.getUserId();
    }
}
