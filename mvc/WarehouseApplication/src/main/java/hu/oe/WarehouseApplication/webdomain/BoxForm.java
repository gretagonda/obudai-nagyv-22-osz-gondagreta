package hu.oe.WarehouseApplication.webdomain;

import hu.oe.WarehouseApplication.validation.BoxSizeConstraint;
import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

public class BoxForm {

    @NotEmpty(message="You have to provide a storage id")
    private List<String> storageroomid;
    @BoxSizeConstraint
    private String size;
    @NotEmpty(message="You have to specify at least one material")
    private List<Material> materials;
    @NotEmpty(message="You have to specify at least one category")
    private List<Category> categories;

    public BoxForm() {
        materials = new ArrayList<>();
        categories = new ArrayList<>();
    }


    public List<String> getStorageroomid() {
        return storageroomid;
    }

    public void setStorageroomid(List<String> storageroomid) {
        this.storageroomid = storageroomid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public List<Material> getMaterials() {
        return materials;
    }

    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
