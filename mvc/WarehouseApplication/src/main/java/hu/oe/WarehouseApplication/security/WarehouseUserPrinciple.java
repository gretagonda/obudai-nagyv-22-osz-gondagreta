package hu.oe.WarehouseApplication.security;

import hu.oe.WarehouseApplication.persistance.UserType;
import hu.oe.WarehouseApplication.persistance.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WarehouseUserPrinciple implements UserDetails {
    final private User user;

    public WarehouseUserPrinciple(User user) {
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<Role> role = new ArrayList<>();
        if(user.getUserType()== UserType.ADMIN)
        {
            role.add(Role.ROLE_ADMIN);

        }else{
            role.add(Role.ROLE_COSTUMER);
        }

        return role;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    public Long getUserId()
    {return user.getUserId();}

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
