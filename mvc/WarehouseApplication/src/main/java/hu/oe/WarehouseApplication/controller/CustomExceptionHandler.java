package hu.oe.WarehouseApplication.controller;

import hu.oe.WarehouseApplication.exception.ServiceProviderException;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class CustomExceptionHandler {
    @ExceptionHandler(ServiceProviderException.class)
    @ResponseStatus(code= HttpStatus.BAD_GATEWAY)
    public  String handleCustomException(ServiceProviderException ex, Model model)
    {
        model.addAttribute("errorCode", ex.getErrorCode());
        model.addAttribute("errorMessage", ex.getErrorMessage());
        return "app-error";
    }
}
