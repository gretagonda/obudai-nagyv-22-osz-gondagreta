package hu.oe.WarehouseApplication.service;


import hu.oe.WarehouseApplication.exception.ServiceProviderException;
import hu.oe.WarehouseApplication.persistance.UserType;
import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.persistance.entity.User;
import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.persistance.repository.BoxRepository;
import hu.oe.WarehouseApplication.persistance.repository.UserRepository;
import hu.oe.WarehouseApplication.persistance.repository.StorageRoomRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;


@org.springframework.stereotype.Service
public class WarehouseService{

    private final BoxRepository boxRepository;
    private final UserRepository userRepository;
    private final StorageRoomRepository storageRoomRepository;
    private IsLogged isLogged;


    @Autowired
    public WarehouseService(BoxRepository boxRepository, UserRepository userRepository, StorageRoomRepository storageRoomRepository, IsLogged isLogged)
    {
        this.boxRepository = boxRepository;
        this.userRepository = userRepository;
        this.storageRoomRepository = storageRoomRepository;
        this.isLogged = isLogged;
    }

    public IsLogged getIsLogged() {
        return isLogged;
    }

    public void setIsLogged(IsLogged isLogged) {
        this.isLogged = isLogged;
    }

    public Iterable<StorageRoom> getAllStorageRooms()
    {
        return  storageRoomRepository.findAll();
    }

    public Iterable<StorageRoom> getMyStorageRooms(User costumer)
    {
       return  storageRoomRepository.findAllByUser(costumer);
    }

    public Iterable<StorageRoom> getUserStorageRooms(Long id)
    {
        return getMyStorageRooms(userRepository.findById(id).get());
    }

    public User getUserById(Long id)
    {
        return userRepository.findById(id).get();
    }

    public List<String> getStorageRoomIdsAsArray(Long id)
    {
        List<String> ids = new ArrayList<>();

        Iterable<StorageRoom> usersRooms = getUserStorageRooms(id);

        for(StorageRoom room : usersRooms)
        {
            ids.add(Long.toString(room.getStorageRoomId()));
        }

        return  ids;
    }

    public Long getLoggedInUserId()
    {
        return  isLogged.getLoggedInUserId();
    }
    public List<Box> getCostumersBoxes(User costumer)
    {
        List<Box> boxes = new ArrayList<>();
        Iterable<Box> allBoxes = boxRepository.findAll();
        for(Box box: allBoxes)
        {
            if(!box.getUserId().equals(0L) && box.getUserId().equals(costumer.getUserId()))
            {
                boxes.add(box);
            }
        }

        return  boxes;
    }


    public  List<Box> getBoxesByCostumer(Long id)
    {
        return getCostumersBoxes(userRepository.findById(id).get());
    }
    public void getStorageRoomsToCostumers()
    {
        Iterable<User> costumers = userRepository.findAll();
        Iterable<StorageRoom> storageRooms = storageRoomRepository.findAll();

        for(User c : costumers)
        {
            for(StorageRoom sr : storageRooms)
            {
                if(sr.getUser()!=null) {
                    if (c.getUserId().equals(sr.getUserId())) {
                        isLogged.getUser().addStorageRoom(sr);
                        User cost = userRepository.findById(c.getUserId()).get();
                        cost.addStorageRoom(sr);
                        userRepository.save(cost);
                    }
                }
            }
        }


    }

    public void encriptUserPasswors()
    {
        Iterable<User> costumers = userRepository.findAll();

        for(User c : costumers)
        {
            String pw = c.getPassword();
            c.setPassword(pw);
            userRepository.save(c);

        }

    }

    public void setUserType()
    {
        Iterable<User> costumers = userRepository.findAll();

        for(User c : costumers)
        {
            c.setUserType(UserType.COSTUMER);
            userRepository.save(c);

        }

    }

    public User getUserByUsername(String username)
    {
        User user = new User();
        for(User c : userRepository.findAll())
        {
            if(c.getUsername().equals(username))
            {
                user = c;
            }
        }

        return user;
    }


    public User getLoggedInCostumer() {
        return isLogged.getUser();

    }

    public void rentStorageRoom(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        if(sr.isFree()==true) {
            sr.setFree(false);
            //sr.setCostumer(isLogged.getCostumer());

            sr.setUser(userRepository.findById(getLoggedInCostumer().getUserId()).get());
            storageRoomRepository.save(sr);
            //isLogged.getCostumer().addStorageRoom(sr);
            //Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
            User cost = userRepository.findById(getLoggedInCostumer().getUserId()).get();
            cost.addStorageRoom(sr);


            try {
                storageRoomRepository.save(sr);
                userRepository.save(cost);

            } catch (Exception e) {
                throw new ServiceProviderException("Save-1", "Unable to rent storage room");
            }

        }

    }

    public void cancelStorageRoomRenting(Long storageRoomId) {
        StorageRoom sr = storageRoomRepository.findById(storageRoomId).get();
        //isLogged.getCostumer().removeStorageRoomById(storageRoomId);
        User c = userRepository.findById(getLoggedInCostumer().getUserId()).get();
        c.removeStorageRoomById(storageRoomId);
        //Costumer cst = costumerRepository.findById(isLogged.getCostumer().getCostumerId()).get();
        //cst.removeStorageRoomById(storageRoomId);
        sr.setFree(true);
        sr.setUser(null);

        try{
            storageRoomRepository.save(sr);
            userRepository.save(c);

        }catch (Exception e)
        {
            throw new ServiceProviderException("Save-1","Unable to cancel storage room renting");
        }



    }

    public void saveBox(Box box)
    {
        try{
            boxRepository.save(box);

        }catch (Exception e)
        {
            throw new ServiceProviderException("Save-1","Unable to save box");
        }
    }

    public void removeBox(Long boxId) {
        Box bx = boxRepository.findById(boxId).get();
        boxRepository.delete(bx);

        User c = userRepository.findById(getLoggedInCostumer().getUserId()).get();

        for (int i = 0; i < c.getStorageRooms().size(); i++) {
            for (int j = 0; j < c.getStorageRooms().get(i).getBoxes().size(); j++) {
                Box b = c.getStorageRooms().get(i).getBoxes().get(j);
                if (b.getBoxId().equals(boxId)) {
                    try {
                        c.getStorageRooms().get(i).removeBox(b);


                    } catch (Exception e) {
                        throw new ServiceProviderException("Remove-1", "Unable to remove box");
                    }
                }
            }
        }

    }

    public  StorageRoom getStorageRoomById(Long id)
    {
        return storageRoomRepository.findById(id).get();
    }

    public boolean getStorageRoomByIdForValidation(Long id)
    {
        return storageRoomRepository.existsById(id);
    }
}
