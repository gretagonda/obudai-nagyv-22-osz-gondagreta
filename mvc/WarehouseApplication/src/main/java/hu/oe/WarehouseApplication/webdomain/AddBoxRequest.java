package hu.oe.WarehouseApplication.webdomain;

public class AddBoxRequest {
    private String storageroom_id;
    private String size;
    private String materials;
    private String categories;


    public String getStorageroom_id() {
        return storageroom_id;
    }

    public void setStorageroom_id(String storageroom_id) {
        this.storageroom_id = storageroom_id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMaterials() {
        return materials;
    }

    public void setMaterials(String materials) {
        this.materials = materials;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }
}
