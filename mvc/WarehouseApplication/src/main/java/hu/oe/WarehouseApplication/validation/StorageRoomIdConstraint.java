package hu.oe.WarehouseApplication.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = StorageRoomIdValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface StorageRoomIdConstraint {
    String message() default "Invalid storage room id";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
