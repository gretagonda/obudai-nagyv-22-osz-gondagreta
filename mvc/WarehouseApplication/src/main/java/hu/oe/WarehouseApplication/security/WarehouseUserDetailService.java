package hu.oe.WarehouseApplication.security;

import hu.oe.WarehouseApplication.persistance.entity.User;
import hu.oe.WarehouseApplication.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class WarehouseUserDetailService implements UserDetailsService {
    @Autowired
    private WarehouseService warehouseService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = warehouseService.getUserByUsername(username);
        if(user==null)
        {
            throw new UsernameNotFoundException(username);
        }
        warehouseService.getIsLogged().setUser(user);
        return new WarehouseUserPrinciple(user);
    }
}
