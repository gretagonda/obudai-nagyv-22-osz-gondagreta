package hu.oe.WarehouseApplication.webdomain;

public class StorageRoomListView {
    private final Long storageRoom_id;
    private final  String size;
    private final String owner;
    private final boolean isFree;
    private final int numBoxes;

    public StorageRoomListView(Long storageRoom_id, String size, String owner, boolean isFree, int numBoxes) {
        this.storageRoom_id = storageRoom_id;
        this.size = size;
        this.owner = owner;
        this.isFree = isFree;
        this.numBoxes = numBoxes;
    }


    public Long getStorageRoom_id() {
        return storageRoom_id;
    }

    public String getSize() {
        return size;
    }

    public String getOwner() {
        return owner;
    }

    public boolean getIsFree() {
        return isFree;
    }

    public int getNumBoxes() {
        return numBoxes;
    }

}
