package hu.oe.WarehouseApplication;



import hu.oe.WarehouseApplication.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class App {

    private WarehouseService warehouseService;

    @Autowired
    public App(WarehouseService warehouseService )
    {
        this.warehouseService = warehouseService;
    }

    public App()
    {

    }




    public void start() {
        initCostumerList();
    }

    public void initCostumerList()
    {
        warehouseService.getStorageRoomsToCostumers();
        warehouseService.encriptUserPasswors();
        warehouseService.setUserType();

    }

}
