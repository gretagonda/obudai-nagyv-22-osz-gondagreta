package hu.oe.WarehouseApplication.validation;

import hu.oe.WarehouseApplication.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StorageRoomIdValidator  implements ConstraintValidator<hu.oe.WarehouseApplication.validation.StorageRoomIdConstraint, String> {
    @Autowired
    private WarehouseService warehouseService;

    @Override
    public boolean isValid(String idField, ConstraintValidatorContext ctx) {
        boolean valid = true;
        if(idField.equals(""))
        {
            valid=false;
        }else {
            if (!idField.matches("[0-9]")) {
                valid = false;
            }else {

                boolean room = warehouseService.getStorageRoomByIdForValidation(Long.parseLong(idField));

                if (!room) {
                    valid = false;
                }
            }
        }

        return valid;
    }
}
