package hu.oe.WarehouseApplication.persistance.entity;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.Size;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Box {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Long boxId;

    @Embedded
    private Size size;

    @ManyToOne
    @JoinColumn(name="storageRoomId")
    private hu.oe.WarehouseApplication.persistance.entity.StorageRoom storageRoom;

    @ManyToOne
    @JoinColumn(name="userId")
    private hu.oe.WarehouseApplication.persistance.entity.User user;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass= Material.class)
    @Enumerated(EnumType.STRING)
    private List<Material> materials;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass= Category.class)
    @Enumerated(EnumType.STRING)
    private List<Category> categories;

    public Box()
    {
        storageRoom = new hu.oe.WarehouseApplication.persistance.entity.StorageRoom();
        user = new hu.oe.WarehouseApplication.persistance.entity.User();
        materials = new ArrayList<Material>();
        categories = new ArrayList<Category>();
    }

    public Long getBoxId() {
        return boxId;
    }
    public void setBoxId(Long boxId) {
        this.boxId = boxId;
    }
    public Size getSize() {
        return size;
    }
    public void setSize(Size size) {
        this.size = size;
    }
    public void setSize2(int x, int y) {
        Size s = new Size();
        s.setX(x);
        s.setY(y);
        this.size = s;
    }

    public List<Material> getMaterials() {
        return materials;
    }
    public void setMaterials(List<Material> materials) {
        this.materials = materials;
    }
    public List<Category> getCategories() {
        return categories;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void addMaterials(Material material) {
        this.materials.add(material);
    }

    public void addCategories(Category category)
    {
        this.categories.add(category);
    }

    public hu.oe.WarehouseApplication.persistance.entity.StorageRoom getStorageRoom() {
        return storageRoom;
    }

    public void setStorageRoom(hu.oe.WarehouseApplication.persistance.entity.StorageRoom storageRoom) {
        this.storageRoom = storageRoom;
    }

    public Long getStorageRoomId() {
        return storageRoom.getStorageRoomId();
    }

    public void setStorageRoomId(Long storageRoomId) {
        this.storageRoom.setStorageRoomId(storageRoomId);
    }


    public hu.oe.WarehouseApplication.persistance.entity.User getUser() {
        return user;
    }

    public void setUser(hu.oe.WarehouseApplication.persistance.entity.User user) {
        this.user = user;
    }

    public Long getUserId() {
        return user.getUserId();
    }

    public void setUserId(Long userId) {
        this.user.setUserId(userId);
    }


}
