package hu.oe.WarehouseApplication.controller;

import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.webdomain.RentStorageRoomRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RentStorageRoomController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RentStorageRoomController.class);

    @Autowired
    private WarehouseService warehouseService;

    @PostMapping("rentStorageRoom")
    public String rentStorageRoom(@Valid RentStorageRoomRequest rentStorageRoomRequest, BindingResult bindingResult)
    {
        if(bindingResult.hasErrors())
        {
            return "redirect:storagerooms";
        }
        String id = rentStorageRoomRequest.getStorageRoomId();
        warehouseService.rentStorageRoom(Long.parseLong(id));

        LOGGER.info("StorageRoom [{}] is successfully rented.", id);

        return  "redirect:storagerooms";
    }


    @PostMapping("cancelstorageroom")
    public String cancelStorageRoom(String storageRoomId)
    {
        //String id = rentStorageRoomRequest.getStorageRoomId();
        warehouseService.cancelStorageRoomRenting(Long.parseLong(storageRoomId));

        LOGGER.info("StorageRoom [{}] is successfully cancelled.", storageRoomId);

        return  "redirect:mystoragerooms";
    }

}
