package hu.oe.WarehouseApplication.persistance.entity;
import hu.oe.WarehouseApplication.persistance.Size;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StorageRoom {
    @Id
    @GeneratedValue
    private Long storageRoomId;
    private boolean isFree;

    @ManyToOne
    @JoinColumn(name="userId", nullable = true)
    private hu.oe.WarehouseApplication.persistance.entity.User user;

    @ElementCollection
    @OneToMany(mappedBy="storageRoom", fetch = FetchType.EAGER)
    private List<hu.oe.WarehouseApplication.persistance.entity.Box> boxes;
    @Embedded
    private Size size;

    public StorageRoom()
    {
        user = new hu.oe.WarehouseApplication.persistance.entity.User();
        user.setUserId(0L);
        isFree = true;
        boxes=new ArrayList<>();
    }


    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public void setSize2(int x, int y) {
        Size s = new Size();
        s.setX(x);
        s.setY(y);
        this.size = s;
    }

    public Long getStorageRoomId() {
        return storageRoomId;
    }

    public void setStorageRoomId(Long storageRoomId) {
        this.storageRoomId = storageRoomId;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean isFree) {
        this.isFree = isFree;
    }


    public hu.oe.WarehouseApplication.persistance.entity.User getUser() {
        return user;
    }

    public void setUser(hu.oe.WarehouseApplication.persistance.entity.User user) {
        this.user = user;
    }

    public Long getUserId() {
        return user.getUserId();
    }

    public void setUserId(Long userId) {
        this.user.setUserId(userId);
    }

    public List<hu.oe.WarehouseApplication.persistance.entity.Box> getBoxes() {
        return boxes;
    }


    public void setBoxes(List<hu.oe.WarehouseApplication.persistance.entity.Box> boxes) {
        this.boxes = boxes;
    }

    public void addBox(hu.oe.WarehouseApplication.persistance.entity.Box box) {
        this.boxes.add(box);
    }

    public void removeBox(hu.oe.WarehouseApplication.persistance.entity.Box box) {
        this.boxes.remove(boxes.indexOf(box));
    }
}
