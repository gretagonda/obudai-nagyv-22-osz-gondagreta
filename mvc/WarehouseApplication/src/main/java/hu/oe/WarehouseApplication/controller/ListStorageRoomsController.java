package hu.oe.WarehouseApplication.controller;

import hu.oe.WarehouseApplication.persistance.entity.StorageRoom;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.transformer.StorageRoomsToListView;
import hu.oe.WarehouseApplication.webdomain.StorageRoomListView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ListStorageRoomsController {

    @Autowired
    private WarehouseService warehouseService;

    @GetMapping("/")
    public String home()
    {
        return "redirect:storagerooms";
    }

    @GetMapping("/storagerooms")
    public String listStorageRooms(Model model)
    {

        Iterable<StorageRoom>  storageRooms = warehouseService.getAllStorageRooms();
        StorageRoomsToListView transformStorageRoomsToListView = new StorageRoomsToListView();

        List<StorageRoomListView> storageRoomsListView=transformStorageRoomsToListView.transformStorageRoomsToViewList(storageRooms);

        model.addAttribute("storagerooms", storageRoomsListView);

        return "storagerooms";

    }

    @GetMapping("/mystoragerooms")
    public String listMyStorageRooms(Model model)
    {


        Iterable<StorageRoom>  storageRooms = warehouseService.getUserStorageRooms(warehouseService.getLoggedInCostumer().getUserId());
        StorageRoomsToListView transformStorageRoomsToListView = new StorageRoomsToListView();

        List<StorageRoomListView> storageRoomsListView=transformStorageRoomsToListView.transformStorageRoomsToViewList(storageRooms);

        model.addAttribute("mystoragerooms", storageRoomsListView);

        return "mystoragerooms";

    }


}
