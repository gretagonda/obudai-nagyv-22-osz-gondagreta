package hu.oe.WarehouseApplication.controller;

import hu.oe.WarehouseApplication.persistance.Category;
import hu.oe.WarehouseApplication.persistance.Material;
import hu.oe.WarehouseApplication.persistance.entity.Box;
import hu.oe.WarehouseApplication.service.WarehouseService;
import hu.oe.WarehouseApplication.transformer.BoxesToAddFormViewAndBack;
import hu.oe.WarehouseApplication.transformer.BoxesToListView;
import hu.oe.WarehouseApplication.webdomain.BoxForm;
import hu.oe.WarehouseApplication.webdomain.BoxListView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;
import java.util.List;

@Controller
public class ListBoxesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ListBoxesController.class);

    @Autowired
    WarehouseService warehouseService;

    @Autowired
    BoxesToListView boxesToListView;

    @Autowired
    private BoxesToAddFormViewAndBack BoxesToAddFormViewAndBack;


    @GetMapping("/boxes")
    public String listMyStorageRooms(Model model)
    {
        Iterable<Box> boxes = warehouseService.getBoxesByCostumer(warehouseService.getLoggedInCostumer().getUserId());

        List<BoxListView> boxListView=boxesToListView.transformBoxesToListView(boxes);
        List<String> userRooms = warehouseService.getStorageRoomIdsAsArray(warehouseService.getLoggedInCostumer().getUserId());
        BoxForm addBoxForm = BoxesToAddFormViewAndBack.transformUsersBoxesToAddBoxFormView(warehouseService.getLoggedInCostumer());
        if(userRooms.size()!=0) {
            model.addAttribute("addboxform", addBoxForm);

            model.addAttribute("boxes", boxListView);

            return "boxes";
        }
        else{

            model.addAttribute("boxes", boxListView);

            return "boxesifempty";
        }


    }

    @PostMapping("removebox")
    public String cancelStorageRoom(String boxId)
    {
        //String id = rentStorageRoomRequest.getStorageRoomId();
        warehouseService.removeBox(Long.parseLong(boxId));

        LOGGER.info("Box [{}] is successfully removed.", boxId);

        return  "redirect:boxes";
    }

    @ModelAttribute("storagerooms")
    public List<String> getStorageRooms(){
        List<String> rooms = warehouseService.getStorageRoomIdsAsArray(warehouseService.getLoggedInCostumer().getUserId());
        return rooms;
    }

    @ModelAttribute("materials")
    public List<Material> getMaterials(){
        Material[] materialsArray = Material.values();
        List<Material> materials = Arrays.asList(materialsArray);
        return materials;
    }

    @ModelAttribute("categories")
    public List<Category> getCategories(){
        Category[] categoriesArray = Category.values();
        List<Category> categories = Arrays.asList(categoriesArray);
        return categories;
    }
}
