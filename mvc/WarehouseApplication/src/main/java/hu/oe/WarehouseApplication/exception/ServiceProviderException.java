package hu.oe.WarehouseApplication.exception;

public class ServiceProviderException extends RuntimeException {
    private final String errorCode;
    private final String errorMessage;

    public ServiceProviderException(String errorCode, String errorMessage) {
        super();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
